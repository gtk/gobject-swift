//
//  Signal.swift
//  gobject-swift
//
//  Created by Alexander Zalutskiy on 22.03.16.
//
//

import CGTK

private func g_signal_connect (_ instance: gpointer,
                               _ detailed_signal: String,
                               _ c_handler: UnsafePointer<Void>,
                               _ data: UnsafeMutablePointer<Void>) {
	g_signal_connect_data (instance, detailed_signal, unsafeBitCast(c_handler, to: GCallback.self), data, nil, GConnectFlags(0))
}

public class SignalData<O: Object, F> {
	public let obj: O
	public let function: F

	init(obj: O, function: F) {
		self.obj = obj
		self.function = function
	}
}

public class Signal<F, O: Object, NativeFunc> {
	private let c_handler: NativeFunc
	private let signal: String
	private weak var obj: O!
	
	private var datas = [SignalData<O, F>]()
	
	public init(obj: O, signal: String, c_handler: NativeFunc) {
		self.obj = obj
		self.signal = signal
		self.c_handler = c_handler
	}
	
	public func connect(function: F) {
		let data = SignalData<O, F>(obj: obj, function: function)
		
		datas.append(data)
		
		let method = unsafeBitCast(c_handler, to: UnsafePointer<Void>.self)
		let c_data = unsafeBitCast(data, to: UnsafeMutablePointer<gpointer>.self)

		g_signal_connect(obj.n_Object, signal, method, c_data)
	}
}