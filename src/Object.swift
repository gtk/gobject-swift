//
//  Object.swift
//  GObject-swift
//
//  Created by Alexander Zalutskiy on 22.03.16.
//
//

import CGTK

public class Object {
	public var n_Object: UnsafeMutablePointer<GObject>
	
	public init(n_Object: UnsafeMutablePointer<GObject>) {
		self.n_Object = n_Object
	}
	
	public func gTypeFromInstance() -> GType {
		let n_Instance = unsafeBitCast(n_Object, to: UnsafePointer<GTypeInstance>.self).pointee
		let n_Class = unsafeBitCast(n_Instance.g_class, to: UnsafePointer<GTypeClass>.self)
		return n_Class.pointee.g_type
	}
}